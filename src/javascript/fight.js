export function fight(firstFighter, secondFighter) {
  var winner;
  if(getDamage(firstFighter) > getDamage(secondFighter)){
    winner = secondFighter;
  }else{
    winner = firstFighter;
  }
  return winner
}

export function getDamage(attacker, enemy) {
  const damage = getHitPower(attacker) - getBlockPower(enemy);
  return damage;
}

export function getHitPower(fighter) {
  var criticalHitChance = (Math.random() * 2) + 1;
  fighter.power = fighter.attack * criticalHitChance;
  return fighter.power;
}

export function getBlockPower(fighter) {
  var dodgeChance = (Math.random() * 2) + 1;
  fighter.blockPower = fighter.defense * dodgeChance;
  return fighter.blockPower;
}
